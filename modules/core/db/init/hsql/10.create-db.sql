-- begin UNTITLED2_CREW
create table UNTITLED2_CREW (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CREWNAME varchar(255),
    --
    primary key (ID)
)^
-- end UNTITLED2_CREW
-- begin UNTITLED2_PERSON
create table UNTITLED2_PERSON (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    SURNAME varchar(255),
    CREW_ID varchar(36),
    --
    primary key (ID)
)^
-- end UNTITLED2_PERSON
-- begin UNTITLED2_TRANSPORT
create table UNTITLED2_TRANSPORT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NUMBER_ integer not null,
    CREW_ID varchar(36),
    ROUTE_ID varchar(36),
    --
    primary key (ID)
)^
-- end UNTITLED2_TRANSPORT
-- begin UNTITLED2_CLIENT
create table UNTITLED2_CLIENT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    ROUTE_ID varchar(36),
    --
    primary key (ID)
)^
-- end UNTITLED2_CLIENT
-- begin UNTITLED2_ROUTE
create table UNTITLED2_ROUTE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    ROUTE_NAME varchar(255) not null,
    --
    primary key (ID)
)^
-- end UNTITLED2_ROUTE
