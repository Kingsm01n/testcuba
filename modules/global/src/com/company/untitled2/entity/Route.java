package com.company.untitled2.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;

@NamePattern("%s|routeName")
@Table(name = "UNTITLED2_ROUTE")
@Entity(name = "untitled2_Route")
public class Route extends StandardEntity {
    private static final long serialVersionUID = -5849347987148492891L;

    @NotNull
    @Column(name = "ROUTE_NAME", nullable = false, unique = true)
    protected String routeName;

    @OneToMany(mappedBy = "route")
    protected List<Transport> transport;

    public List<Transport> getTransport() {
        return transport;
    }

    public void setTransport(List<Transport> transport) {
        this.transport = transport;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }
}