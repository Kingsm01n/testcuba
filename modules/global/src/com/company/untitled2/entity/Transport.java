package com.company.untitled2.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@NamePattern("%s|number")
@Table(name = "UNTITLED2_TRANSPORT")
@Entity(name = "untitled2_Transport")
public class Transport extends StandardEntity {
    private static final long serialVersionUID = 5434405351850901651L;

    @NotNull
    @Column(name = "NUMBER_", nullable = false, unique = true)
    protected Integer number;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREW_ID")
    protected Crew crew;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROUTE_ID")
    protected Route route;

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Crew getCrew() {
        return crew;
    }

    public void setCrew(Crew crew) {
        this.crew = crew;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}