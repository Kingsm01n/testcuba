package com.company.untitled2.web.screens.client;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Client;

@UiController("untitled2_Client.browse")
@UiDescriptor("client-browse.xml")
@LookupComponent("clientsTable")
@LoadDataBeforeShow
public class ClientBrowse extends StandardLookup<Client> {
}