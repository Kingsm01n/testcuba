package com.company.untitled2.web.screens.client;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Client;

@UiController("untitled2_Client.edit")
@UiDescriptor("client-edit.xml")
@EditedEntityContainer("clientDc")
@LoadDataBeforeShow
public class ClientEdit extends StandardEditor<Client> {
}