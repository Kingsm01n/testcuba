package com.company.untitled2.web.screens.crew;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Crew;

@UiController("untitled2_Crew.browse")
@UiDescriptor("crew-browse.xml")
@LookupComponent("crewsTable")
@LoadDataBeforeShow
public class CrewBrowse extends StandardLookup<Crew> {
}