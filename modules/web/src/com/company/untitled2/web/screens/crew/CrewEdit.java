package com.company.untitled2.web.screens.crew;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Crew;

@UiController("untitled2_Crew.edit")
@UiDescriptor("crew-edit.xml")
@EditedEntityContainer("crewDc")
@LoadDataBeforeShow
public class CrewEdit extends StandardEditor<Crew> {
}