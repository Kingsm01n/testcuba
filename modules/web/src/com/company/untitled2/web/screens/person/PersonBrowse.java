package com.company.untitled2.web.screens.person;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Person;

@UiController("untitled2_Person.browse")
@UiDescriptor("person-browse.xml")
@LookupComponent("personsTable")
@LoadDataBeforeShow
public class PersonBrowse extends StandardLookup<Person> {
}