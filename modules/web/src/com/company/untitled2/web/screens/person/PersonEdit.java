package com.company.untitled2.web.screens.person;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Person;

@UiController("untitled2_Person.edit")
@UiDescriptor("person-edit.xml")
@EditedEntityContainer("personDc")
@LoadDataBeforeShow
public class PersonEdit extends StandardEditor<Person> {
}