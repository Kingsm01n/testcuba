package com.company.untitled2.web.screens.route;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Route;

@UiController("untitled2_Route.browse")
@UiDescriptor("route-browse.xml")
@LookupComponent("routesTable")
@LoadDataBeforeShow
public class RouteBrowse extends StandardLookup<Route> {
}