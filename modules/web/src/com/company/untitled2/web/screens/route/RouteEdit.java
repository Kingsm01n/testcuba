package com.company.untitled2.web.screens.route;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Route;

@UiController("untitled2_Route.edit")
@UiDescriptor("route-edit.xml")
@EditedEntityContainer("routeDc")
@LoadDataBeforeShow
public class RouteEdit extends StandardEditor<Route> {
}