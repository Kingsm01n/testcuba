package com.company.untitled2.web.screens.transport;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Transport;

@UiController("untitled2_Transport.browse")
@UiDescriptor("transport-browse.xml")
@LookupComponent("transportsTable")
@LoadDataBeforeShow
public class TransportBrowse extends StandardLookup<Transport> {
}