package com.company.untitled2.web.screens.transport;

import com.haulmont.cuba.gui.screen.*;
import com.company.untitled2.entity.Transport;

@UiController("untitled2_Transport.edit")
@UiDescriptor("transport-edit.xml")
@EditedEntityContainer("transportDc")
@LoadDataBeforeShow
public class TransportEdit extends StandardEditor<Transport> {
}